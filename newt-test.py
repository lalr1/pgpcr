#!/usr/bin/python3

import os
import json

from pgpcr import newt as ui, tran

class DiskSubmenuItem:
    def __init__(self, main_menu_text, menu_title, menu_description):
        self.main_menu_text = main_menu_text
        self.menu_title = menu_title
        self.menu_description = menu_description

    def run_menu(self, screen, removeable_only=False):
        if removeable_only:
            rm_label = 'Show all block devices'
        else:
            rm_label = 'Show only removeable devices'
        (labels, items) = self.menu_items(removeable_only)
        if labels == []:
            ui.error(screen, 'No suitable devices found')
            return
        (button, selection) = ui.LCW(screen, self.menu_title,
                                     self.menu_description,
                                     labels,
                                     buttons = [(rm_label, 'toggle'),
                                                ('Cancel', 'cancel')])
        if button == 'cancel':
            return
        elif button == 'toggle':
            self.run_menu(screen, not removeable_only)
        else:
            self.run_command(screen,items[selection])

    def menu_items(self, removeable_only):
        items = self.get_items(removeable_only)
        return ([self.format_label(i) for i in items],
                items)

class DisksMenu(DiskSubmenuItem):
    def get_items(self, removeable_only):
        return [d for d in get_blkdict()
                if not removeable_only or d['hotplug']]

class VolumesMenu(DiskSubmenuItem):
    def get_items(self, removeable_only):
        # I don't think you can splice a list into a comprehension.
        parts=[]
        for d in get_blkdict():
            if not removeable_only or d['hotplug']:
                if 'children' in d:
                    for p in d['children']:
                        parts.append(p)
                else:
                    parts.append(d)
        return parts

    def format_label(self, item):
         if item['fstype'] is None:
             return item['path']
         else:
             return f'{item["path"]} ({item["fstype"]})'
    
class PartitionMenu(DisksMenu):
    def format_label(self, item):
        if item['vendor'] is None:
            return item['path'] 
        else:
            return f'{item["path"]} ({item["vendor"]} {item["model"]})'

    def run_command(self, screen, item):
        external_command(screen, ['cfdisk', item['path']])

class EncryptMenu(VolumesMenu):
    def run_command(self, screen, item):
        external_command(screen, ['cryptsetup', item['path']])

class FormatMenu(VolumesMenu):
    def run_command(self, screen, item):
        external_command(screen, ['mkfs', item['path']])

class MountMenu(VolumesMenu):
    def run_command(self, screen, item):
        external_command(screen, ['mount', item['path']])

def get_blkdict():
    with os.popen('lsblk --json --tree --output-all') as pipe:
        return json.load(pipe)['blockdevices']
 
def external_command(screen, cmd):
    screen.finish()
    print(f'Pretending to run {cmd}')
    input('Press Return to continue')

def main_menu():
    menus = [PartitionMenu('Edit the partition table of a disk',
                           'Partition a disk',
                           'Select a disk to partition'),
             EncryptMenu('Set up encryption on a device',
                         'Encrypt a partition',
                         'Select a partition to encrypt'),
             FormatMenu('Format a device',
                        'Format a partition',
                        'Select a partition to format'),
             MountMenu('Mount a device',
                       'Mount a device',
                       'Select a device to mount')]
    menu_items = [(m.main_menu_text, m) for m in menus]
    menu_items.append(('Quit', None))
#   default_item = 1
    while True:
        # The submenus might call screen.finish() in order to run an
        # external command with terminal output, so re-initialize
        # here.  Newt will segfault if we don't!
        screen = ui.Screen()
        # This function doesn't have an option to select the
        # default (initially-highlighted) item.
        choice = ui.LCM(screen, 'Prepare a disk',
                        'Select an operation',
                        menu_items)
        if choice is None:
            break
        else:
#           default_item = menus.index(choice)
            choice.run_menu(screen)
    screen.finish()

if __name__ == '__main__':
    tran.init(os.environ["LANG"])
    main_menu()

# Local Variables:
# python-shell-interpreter: "/usr/bin/python3"
# End:
