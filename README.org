PGP Clean Room, forked from
[[https://salsa.debian.org/tookmund-guest/pgpcr]].

First read the original readme file in README.orig.md.

* Changes
  So far I have commented out some lines in ~sbin/pgp-clean-room~ to
  stop it from doing silly things when run for testing purposes.

  See also a prototype for a disk-operation menu in ~newt-test.py~

* Testing
  To test the main application without installing anything, Python
  needs to know where the ~pgpcr~ library is, and GPG needs to be told
  not to clobber your home directory.  Running it from the top level
  of the repository:

  ~HOME=/tmp PYTHONPATH=. sbin/pgp-clean-room~

  *The application will interfere with your GPG keyring* if you don't
   do this.
